TEST_PACKAGES := $(shell go list ./... | grep -v vendor | grep -v fakes)

install:
	@echo "Installing local development dependencies"
	go get github.com/kardianos/govendor
	govendor sync

test:
	go test -v -cover $(TEST_PACKAGES)
